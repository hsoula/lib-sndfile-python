#!/usr/bin/env python
#


import os
import sys

from distutils.core import setup,Extension
from distutils.command.clean import clean
from distutils.util import get_platform
from distutils.dir_util import remove_tree
from distutils.sysconfig import get_python_lib

VERSION = "1.0.0"
DESCRIPTION = "Builds Mac OS X installer packages from distutils"
LONG_DESCRIPTION = """
libsndfile-python is a python wrapper of the library sndfile.

It includes a numpy support in addition to more classical
python data structure.

This is a new installment of the original libsndfile-python from Rob Melby

H. Soula
"""


try:
	import numpy
	have_numpy = 1
except ImportError:
	have_numpy = 0

define_macros = []
inc_dir = []

platform=get_platform()

print os.path.join(get_python_lib(plat_specific=1))
if (have_numpy):
	define_macros.append(('HAVE_NUMPY', 1))
	numpydir = os.path.join(get_python_lib(plat_specific=1), 'numpy/core/include/')
	inc_dir.append(numpydir)
link_args = []
libraries=[]

if platform.startswith("macosx"):
	#	link_args.extend(['libsndfile.a'])
	link_args.extend(['-framework', 'Python'])
	libraries.append('sndfile')
else:
	libraries.append('python')
	libraries.append('sndfile')

ext_list = ([ Extension(name='sndfile._libsndfile',
                        sources =  ['sndfile/sndfile_module.c'],
                        define_macros=define_macros,
                        libraries=libraries,
			include_dirs=inc_dir,
			extra_link_args=link_args,
                        )
              ])

setup(name="libsndfile-python",
      version=VERSION,
      description="Python wrappers for libsndfile",
      long_description=LONG_DESCRIPTION,
      author="Hedi Soula / Rob Melby [original author] ",
      author_email="hsoula@gmail.com / rm@arcsin.org [original]",
      url="http://code.google.com/p/libsndfile-python/",
      license="LGPL",
      ext_modules=ext_list,
      packages=['sndfile'],
      packages_dir={'sndfile':'sndfile'},
      packages_data={'sndfile' : ['examples/*.py','./*.py']},
)

