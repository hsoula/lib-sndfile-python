#
# libsndfile-python  (a python wrapper for libsndfile)
# Copyright (C) 2003  RM (rm@arcsin.org)
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

import _libsndfile
from command import *

SF_FORMAT_WAV			= 0x010000
SF_FORMAT_AIFF			= 0x020000
SF_FORMAT_AU			= 0x030000
SF_FORMAT_RAW			= 0x040000
SF_FORMAT_PAF			= 0x050000
SF_FORMAT_SVX			= 0x060000
SF_FORMAT_NIST			= 0x070000
SF_FORMAT_VOC			= 0x080000
SF_FORMAT_IRCAM			= 0x0A0000
SF_FORMAT_W64			= 0x0B0000
SF_FORMAT_MAT4			= 0x0C0000
SF_FORMAT_MAT5			= 0x0D0000
# Subtypes from here on. 
SF_FORMAT_PCM_S8		= 0x0001
SF_FORMAT_PCM_16		= 0x0002
SF_FORMAT_PCM_24		= 0x0003
SF_FORMAT_PCM_32		= 0x0004
SF_FORMAT_PCM_U8		= 0x0005
SF_FORMAT_FLOAT			= 0x0006
SF_FORMAT_DOUBLE		= 0x0007
SF_FORMAT_ULAW			= 0x0010
SF_FORMAT_ALAW			= 0x0011
SF_FORMAT_IMA_ADPCM		= 0x0012
SF_FORMAT_MS_ADPCM		= 0x0013
SF_FORMAT_GSM610		= 0x0020
SF_FORMAT_VOX_ADPCM		= 0x0021
SF_FORMAT_G721_32		= 0x0030
SF_FORMAT_G723_24		= 0x0031
SF_FORMAT_G723_40		= 0x0032
SF_FORMAT_DWVW_12		= 0x0040
SF_FORMAT_DWVW_16		= 0x0041
SF_FORMAT_DWVW_24		= 0x0042
SF_FORMAT_DWVW_N		= 0x0043

SF_ENDIAN_FILE			= 0x00000000
SF_ENDIAN_LITTLE		= 0x10000000
SF_ENDIAN_BIG			= 0x20000000
SF_ENDIAN_CPU			= 0x30000000

SF_FORMAT_SUBMASK		= 0x0000FFFF
SF_FORMAT_TYPEMASK		= 0x0FFF0000
SF_FORMAT_ENDMASK		= 0x30000000

SFM_READ	= 0x10
SFM_WRITE	= 0x20
SFM_RDWR	= 0x30

class SF_INFO:
	def __init__(self, frames = 0, samplerate = 0, channels = 0, 
			   format = 0, sections = 0, seekable = 0):
		self.frames = frames
		self.samplerate = samplerate 
		self.channels = channels
		self.format = format 
		self.sections = sections
		self.seekable = seekable
	# end def
	
	def get_copy(self):
		return SF_INFO(frames = self.frames,
			       samplerate = self.samplerate,
			       channels = self.channels,
			       format = self.format, 
			       sections = self.sections,
			       seekable = self.seekable)
	# end def

	def __repr__(self):
		t1 = (self.frames, self.samplerate, self.channels)
		t2 = (self.format, self.sections, self.seekable)
		return ("<SF_INFO: %i frames, %i HZ, %i channel(s),"%t1+
			" %i format, %i sections, %i seekable>"%t2)
	# end def 
# end class sf_info




class SndFile:
	def __init__(self, path, mode="r", sfinfo=None):
		self._generate_read_write_mappings()
		self.file,self.info = self.open(path, mode, sfinfo)
	# end def

	def _generate_read_write_mappings(self):
		"""
		this pulls in all the wrapped libsnd r/w routines
		and makes a corresponding function alias in this
		instance
		XXX: we should probably do this to the class 
		     prototype only once ...
		"""   
		names = dir(_libsndfile)
		for name in names:
			""" skip the numarray versions of the 
			    wrappers, since those will be taken care of 
			    with their normal counter parts
			"""
			if (name.find("_na_") >= 0) or (name.find("_npy_")>=0):
				continue;
			# end if

			is_read = (name.find("read") >= 0)
			if (is_read or
			    name.find("write") >= 0):
				# makes the assumption that the name 
				# is _wrap_sf_(read|write)_type
				# n0 should be '', n1 should be 'wrap'
				(n0, n1, n2, method_name) = name.split("_", 3)
				na_name = n0+"_"+n1+"_npy_"+n2+"_"+method_name
				# we need to allocate separate storage
				# for the name, otherwise all the 
				# names in the lambda point to the
				# same name string, i thought lexical
				# scoping fixed this so you didn't need
				# a default arg hack ?
				if (is_read):
					func = (lambda x,n=name,na=na_name:
						self._dispatch_read(n,na,x))
				else:
					func = (lambda x,n=name,na=na_name:
						self._dispatch_write(n,na,x))
				# end if

				self.__dict__[method_name] = func
			# end if
		# end for
	# end def


	def _dispatch_write(self, func_name, na_func_name, data):
		""" internal function that checks if the data is a string
		    or numarray and calls the appropriate version 
		    of the wrapper function 

		    alternately we could use the _use_num_array variable?
		"""
		if (type(data) == type("")):
			# use string version
			return _libsndfile.__dict__[func_name](self.file, 
								data, 
								self.info)
		else:
			# use numarray version
			return _libsndfile.__dict__[na_func_name](self.file, 
								data, 
								self.info)
		# end def
		
	# end def

	def _dispatch_read(self, func_name, na_func_name, data):
##		if (not _use_numarray):
##			# use string version
##			return _libsndfile.__dict__[func_name](self.file, 
##								data, 
##								self.info)
##		else:
			# use numarray version
		return _libsndfile.__dict__[na_func_name](self.file, 
								  data, 
								  self.info)
		# end def
	# end def

	def get_info(self):
		return self.info
	# end def 

	def open(self, path, mode, sfinfo=None):
		modeval = 0
	
		if (mode == "rw"):
			modeval = SFM_RDWR
		elif (mode[0] == "w"):
			modeval = SFM_WRITE
		elif (mode[0] == "r"):
			modeval = SFM_READ
		else:
			modeval = -1
			# XXX throw an exception here ?  
		# end if
	
		if (sfinfo == None):
			sfinfo = SF_INFO()
		else:
			# if the sfinfo is passed and we are writing
			# then we need to create a new sfinfo	
			# for the new file we are opening  
			# otherwise we will mess up 
			# the other files sfinfo, when copy the results
			# back into the instance from the sf_open call
			# we cold also just pass back a copy from
			# get_info .... which is better?
			sfinfo = sfinfo.get_copy()
		# end if

		ret = _libsndfile._wrap_sf_open(path, modeval, sfinfo)	
		return ret, sfinfo
	# end def

	def close(self):
		return _libsndfile._wrap_sf_close(self.file)	
	# end def

	def error(self):
		return _libsndfile._wrap_sf_error(self.file)	
	# end def

	def strerror(self):
		return _libsndfile._wrap_sf_strerror(self.file)	
	# end def

	def seek(self, frame_count, whence):
		return _libsndfile._wrap_sf_seek(self.file, 
						 frame_count, 
						 whence)
	# end def

	def command(self, cmd=None, data=None, len=None):
		return command(self, cmd, data, len)

	# end def
# end class Sndfile


def open(path, mode="r", info=None):
	return SndFile(path, mode, info)
# end def



def error_number(i):
	return _libsndfile._wrap_sf_error_number(i)	
# end def


def format_check(sfinfo):
	return _libsndfile._wrap_sf_format_check(sfinfo)	
# end def



# this is set to determine whether we use num_array or not
# we could default to using numarray if it exists?
_use_numarray = 0
_can_use_numarray = 0
# check if we can *possibly* use numarray
try:
	import numarray
	_can_use_numarray = 1
except ImportError:
	pass
# end try



def set_numarray_mode(mode):
	""" if 1, sndfile objects' read methods return numarrays """
	global _can_use_numarray
	global _use_numarray
	if (mode and not _can_use_numarray):
		raise Exception("module numarray is not available")
	else:
		_use_numarray = mode
	# end def
# end def


def get_numarray_mode():
	global _use_numarray
	return _use_numarray
# end def



