#
# libsndfile-python (a python wrapper for libsndfile)
# Copyright (C) 2003  RM (rm@arcsin.org)
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

import _libsndfile

SFC_GET_LIB_VERSION				= 0x1000
SFC_GET_LOG_INFO				= 0x1001
SFC_GET_NORM_DOUBLE				= 0x1010
SFC_GET_NORM_FLOAT				= 0x1011
SFC_SET_NORM_DOUBLE				= 0x1012
SFC_SET_NORM_FLOAT				= 0x1013
SFC_GET_SIMPLE_FORMAT_COUNT			= 0x1020
SFC_GET_SIMPLE_FORMAT				= 0x1021
SFC_GET_FORMAT_INFO				= 0x1028
SFC_GET_FORMAT_MAJOR_COUNT			= 0x1030
SFC_GET_FORMAT_MAJOR				= 0x1031
SFC_GET_FORMAT_SUBTYPE_COUNT			= 0x1032
SFC_GET_FORMAT_SUBTYPE				= 0x1033
SFC_CALC_SIGNAL_MAX				= 0x1040
SFC_CALC_NORM_SIGNAL_MAX			= 0x1041
SFC_CALC_MAX_ALL_CHANNELS			= 0x1042
SFC_CALC_NORM_MAX_ALL_CHANNELS			= 0x1043
SFC_SET_ADD_PEAK_CHUNK				= 0x1050
SFC_UPDATE_HEADER_NOW				= 0x1060
SFC_SET_UPDATE_HEADER_AUTO			= 0x1061
SFC_SET_ADD_DITHER_ON_WRITE			= 0x1070
SFC_SET_ADD_DITHER_ON_READ			= 0x1071
SFC_FILE_TRUNCATE				= 0x1080
SFC_SET_RAW_START_OFFSET			= 0x1090
# Following commands for testing only. 
SFC_TEST_ADD_TRAILING_DATA			= 0x6000
SFC_TEST_IEEE_FLOAT_REPLACE			= 0x6001


# these describe what the meanings of the sf_command parameters
# are , and what should be returned
# they are long on purpose

SF_CMD_LEN_IS_SIZEOF_FORMAT_INFO  =  1
SF_CMD_LEN_IS_SIZEOF_INT          =  2 
SF_CMD_LEN_IS_SIZEOF_SF_COUNT     =  3
SF_CMD_LEN_IS_LEN                 =  4
SF_CMD_LEN_IS_IGNORED             =  22
SF_CMD_LEN_IS_BOOLEAN             =  44

SF_CMD_DATA_IS_NULL               =  5
SF_CMD_DATA_IS_OUT_INT            =  6
SF_CMD_DATA_IS_OUT_STRING         =  7
SF_CMD_DATA_IS_OUT_DOUBLE         =  8
SF_CMD_DATA_IS_OUT_DOUBLE_ARRAY_OF_LENGTH_LEN = 9
SF_CMD_DATA_IS_INOUT_FORMAT_INFO  = 10
SF_CMD_DATA_IS_IN_SF_COUNT        = 11

SF_CMD_RETURN_IS_SUCCESS          = 12
SF_CMD_RETURN_IS_INT_COUNT        = 13
SF_CMD_RETURN_IS_BOOLEAN          = 14
SF_CMD_RETURN_IS_TWO_STRINGS         = 15

def get_len(val):
    """attempts to convert to an into, or uses the default value"""
    try:
        len = int(val)
    except:
        len = 128
    # end try
    return len
# end def


#XXX do better checking
def is_valid_file (f):
    
    if (f == None):
        print ("Error: SFC_GET_LOG_INFO requires an instance "
               "of SndFile as argument.")
        return 0
    else:
        return 1
    # end if
# end def


class SF_FORMAT_INFO:
	def __init__(self, format, name="", extension=""):
		self.format = format
		self.name = name
		self.extension = extension
		return
	# end def
        def __repr__(self):
            return "<SF_FORMAT_INFO: format %i, name %s, extension %s>"%(
                self.format, self.name, self.extension)
        # end def
# end class SF_FORMAT_INFO



## XXX this is going to be really ugly to maintain
##
def command(f=None, cmd=None, data=None, len=None):
    if (cmd==SFC_GET_LIB_VERSION):
        len = get_len(len)
        return _libsndfile._wrap_sf_command(None,
                                            cmd,
                                            SF_CMD_DATA_IS_OUT_STRING,
                                            SF_CMD_RETURN_IS_SUCCESS,
                                            SF_CMD_LEN_IS_LEN,
                                            None,
                                            len)
    elif (cmd==SFC_GET_LOG_INFO):
        len = get_len(len)
        if (not is_valid_file(f)):
            return None
        # end if
        return _libsndfile._wrap_sf_command(f.file,
                                            cmd,
                                            SF_CMD_DATA_IS_OUT_STRING,
                                            SF_CMD_RETURN_IS_SUCCESS,
                                            SF_CMD_LEN_IS_LEN,
                                            None,
                                            len)
    elif (cmd==SFC_CALC_SIGNAL_MAX):
        if (not is_valid_file(f)):
            return None
        # end if
        return _libsndfile._wrap_sf_command(f.file,
                                            cmd,
                                            SF_CMD_DATA_IS_OUT_DOUBLE,
                                            SF_CMD_RETURN_IS_SUCCESS,
                                            SF_CMD_LEN_IS_IGNORED,
                                            None,
                                            0)
    elif (cmd==SFC_CALC_NORM_SIGNAL_MAX):
        if (not is_valid_file(f)):
            return None
        # end if
        return _libsndfile._wrap_sf_command(f.file,
                                            cmd,
                                            SF_CMD_DATA_IS_OUT_DOUBLE,
                                            SF_CMD_RETURN_IS_SUCCESS,
                                            SF_CMD_LEN_IS_IGNORED,
                                            None,
                                            0)
    elif (cmd==SFC_CALC_NORM_MAX_ALL_CHANNELS):
        return _libsndfile._wrap_sf_command(f.file,
                                            cmd,
                              SF_CMD_DATA_IS_OUT_DOUBLE_ARRAY_OF_LENGTH_LEN,
                                            SF_CMD_RETURN_IS_SUCCESS,
                                            SF_CMD_LEN_IS_LEN,
                                            None,
                                            f.info.channels)
    elif (cmd==SFC_SET_NORM_FLOAT or
          cmd==SFC_SET_NORM_DOUBLE or
          cmd==SFC_SET_ADD_PEAK_CHUNK or
          cmd==SFC_UPDATE_HEADER_NOW or
          cmd==SFC_SET_UPDATE_HEADER_AUTO):
        if (len == None) :
            print "WARNING: this function (%i) requires a len argument set to one or zero representing a boolean"%cmd
            return 0 # for failure
        else:
            len = get_len(len)
        # end if
        if (not is_valid_file(f)):
            return 0 # for failure
        # end if
        return _libsndfile._wrap_sf_command(f.file,
                                            cmd,
                                            SF_CMD_DATA_IS_NULL,
                                            SF_CMD_RETURN_IS_SUCCESS,
                                            SF_CMD_LEN_IS_BOOLEAN,
                                            None,
                                            len)
    elif (cmd==SFC_GET_NORM_FLOAT or
          cmd==SFC_GET_NORM_DOUBLE):

        if (not is_valid_file(f)):
            return None # for failure
        # end if

        return _libsndfile._wrap_sf_command(f.file,
                                            cmd,
                                            SF_CMD_DATA_IS_NULL,
                                            SF_CMD_RETURN_IS_BOOLEAN,
                                            SF_CMD_LEN_IS_IGNORED,
                                            None,
                                            0)
    elif (cmd==SFC_GET_SIMPLE_FORMAT_COUNT or
          cmd==SFC_GET_FORMAT_MAJOR_COUNT or
          cmd==SFC_GET_FORMAT_SUBTYPE_COUNT):
        return _libsndfile._wrap_sf_command(None,
                                            cmd,
                                            SF_CMD_DATA_IS_OUT_INT,
                                            SF_CMD_RETURN_IS_SUCCESS,
                                            SF_CMD_LEN_IS_IGNORED,
                                            None,
                                            0)
    elif (cmd == SFC_GET_SIMPLE_FORMAT or
          cmd == SFC_GET_FORMAT_INFO or
          cmd == SFC_GET_FORMAT_MAJOR or
          cmd == SFC_GET_FORMAT_SUBTYPE):

        # here we are going to play fast and loose,
        # passing the function only the int type
        # and having it pass us back just the tuple of strings

        if (not isinstance(data, SF_FORMAT_INFO)):
            print "ERROR: data should be a SF_FORMAT_INFO instance"
            return None
        # end if

        info = data
        ret = _libsndfile._wrap_sf_command(None,
                                            cmd,
                                            SF_CMD_DATA_IS_INOUT_FORMAT_INFO,
                                            SF_CMD_RETURN_IS_TWO_STRINGS,
                                            SF_CMD_LEN_IS_SIZEOF_FORMAT_INFO,
                                            info.format, 0)
        if (ret == None):
            return ret
        # end if
        (name, ext) = ret
        info.name = name
        info.extension = ext
        return info
    elif (cmd == SFC_FILE_TRUNCATE or
          cmd == SFC_SET_RAW_START_OFFSET):
        if (len == None) :
            print "WARNING: this function (%i) requires a len argument set to representing a length argument."%cmd
            return None
        else:
            len = get_len(len)
        # end if
        if (not is_valid_file(f)):
            return None
        # end if
        return _libsndfile._wrap_sf_command(f.file,
                                            cmd,
                                            SF_CMD_DATA_IS_IN_SF_COUNT,
                                            SF_CMD_RETURN_IS_SUCCESS,
                                            SF_CMD_LEN_IS_SIZEOF_SF_COUNT,
                                            len,
                                            0)

    else:
        print "WARNING: no such command %i"%cmd
        return None
    # end if
    
    print "WARNING: command is not implemented"
    return #_libsndfile._wrap_sf_command()	
# end def
