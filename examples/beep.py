
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#


# This code creates a simple file beep.wav that contains several beeps of increasing
# frequencies 


import sys
a=sys.path[-1]
sys.path.insert(0,a)
from numpy import *
import sndfile

## create file info 
rate = 44100
info = sndfile.SF_INFO(samplerate=rate, 
                               channels = 1, 
                               format = (sndfile.SF_FORMAT_WAV|
                                        sndfile.SF_FORMAT_PCM_16), 
                               sections = 1, 
                               seekable = 1)
## open the file 
file = sndfile.open("beep.wav",mode="w",info=info)

## put some data
for y in range(10*3):
    freq = 440 + y*10
    print "frequencies added =",freq,"Hz"
    for x in range(6):
        data = sin(2*pi*freq/float(rate)*arange(0,rate/2))
        ## adds beep
        file.write_double(data)
        ## adds silence
        file.write_double(zeros(rate/2,dtype=double))
## closing
file.close()
