/*
 * libsndfile-python (a python wrapper for libsndfile)
 * Copyright (C) 2003  RM (rm@arcsin.org)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <math.h>
#include <sndfile.h>
#include "Python.h"

#ifdef HAVE_NUMARRAY
#include <numarray/libnumarray.h>
#endif
#ifdef HAVE_NUMPY
#include "numpy/arrayobject.h"
#endif

/* the type of sf_count_t is machine dependent ? 
 * is loff_t 
 * it appears to be machine dependent
 * on *this* platform it looks like a long long
 */
#define PYOBJECT_GET_SF_COUNT(attr)     PyLong_AsLongLong(attr)
#define PYOBJECT_SET_SF_COUNT(val)     PyLong_FromLongLong(val)

static void
copy_py_sfinfo_to_sfinfo(PyObject * py_sfinfo, SF_INFO * sfinfo)
{
  PyObject * attr; 

  if (py_sfinfo == NULL || sfinfo == NULL) 
    { 
      /* XXX error out ? */
      return;
    }

  attr = PyObject_GetAttrString(py_sfinfo, "frames");
  sfinfo->frames = PYOBJECT_GET_SF_COUNT(attr); 
  Py_DECREF(attr);

  attr = PyObject_GetAttrString(py_sfinfo, "channels");
  sfinfo->channels = PyInt_AsLong(attr); 
  Py_DECREF(attr);

  attr = PyObject_GetAttrString(py_sfinfo, "samplerate");
  sfinfo->samplerate = PyInt_AsLong(attr); 
  Py_DECREF(attr);

  attr = PyObject_GetAttrString(py_sfinfo, "format");
  sfinfo->format = PyInt_AsLong(attr);
  Py_DECREF(attr);

  attr = PyObject_GetAttrString(py_sfinfo, "sections");
  sfinfo->sections = PyInt_AsLong(attr);
  Py_DECREF(attr);

  attr = PyObject_GetAttrString(py_sfinfo, "seekable");
  sfinfo->seekable = PyInt_AsLong(attr);
  Py_DECREF(attr);
}

static void
copy_sfinfo_to_py_sf_info(PyObject * py_sfinfo, SF_INFO * sfinfo)
{



  if (py_sfinfo == NULL || sfinfo == NULL) 
    { 
      /* XXX error out ? */
      return;
    }
  PyObject_SetAttrString(py_sfinfo, "frames", 
			 PYOBJECT_SET_SF_COUNT(sfinfo->frames));
  PyObject_SetAttrString(py_sfinfo, "samplerate",
			 PyInt_FromLong(sfinfo->samplerate));  
  PyObject_SetAttrString(py_sfinfo, "channels",
			 PyInt_FromLong(sfinfo->channels));  
  PyObject_SetAttrString(py_sfinfo, "format",
			 PyInt_FromLong(sfinfo->format));
  PyObject_SetAttrString(py_sfinfo, "sections",
			 PyInt_FromLong(sfinfo->sections));
  PyObject_SetAttrString(py_sfinfo, "seekable",
			 PyInt_FromLong(sfinfo->seekable));

}

static PyObject *
_wrap_sf_open(PyObject * self, PyObject * args)
{ 
  /* SNDFILE* sf_open (const char *path, 
   *                   int mode, 
   *                   SF_INFO *sfinfo) 
   */
  char * path;
  int    mode;
  PyObject * py_sfinfo;
  PyObject * ret;
  SF_INFO   sfinfo;
  SNDFILE * sfile;

  if(!PyArg_ParseTuple(args, "siO:sf_open",
		       &path, 
		       &mode,
		       &py_sfinfo)) { 
    return NULL;
  }
  copy_py_sfinfo_to_sfinfo(py_sfinfo, &sfinfo);
  sfile = sf_open (path, mode, &sfinfo);
  copy_sfinfo_to_py_sf_info(py_sfinfo, &sfinfo);
  ret = PyCObject_FromVoidPtr(sfile, NULL);
  return ret;
}

static PyObject * 
_wrap_sf_close(PyObject * self, PyObject * args)
{
  /* int sf_close (SNDFILE *sndfile) ;
   */
  PyObject * cobj;
  SNDFILE * sfile;

  if(!PyArg_ParseTuple(args, "O:sf_close", &cobj)) {
    return NULL;
  }
  
  sfile = PyCObject_AsVoidPtr(cobj);
  return PyInt_FromLong(sf_close(sfile));
}

static PyObject *
_wrap_sf_error(PyObject * self, PyObject * args)
{ 
  /* int		sf_error		(SNDFILE *sndfile) ;
   */
  PyObject * cobj;
  SNDFILE * sfile;

  if(!PyArg_ParseTuple(args, "O:sf_error", &cobj)) {
    return NULL;
  }
  
  sfile = PyCObject_AsVoidPtr(cobj);
  return PyInt_FromLong(sf_error(sfile));
}

static PyObject * 
_wrap_sf_strerror(PyObject * self, PyObject * args)
{
  /* const char* sf_strerror (SNDFILE *sndfile) ;
   */
  PyObject * cobj;
  SNDFILE * sfile;

  if(!PyArg_ParseTuple(args, "O:sf_strerror", &cobj)) {
    return NULL;
  }
  
  /* do we need to copy or deallocate the string? */
  sfile = PyCObject_AsVoidPtr(cobj);
  return PyString_FromString(sf_strerror(sfile));
}

static PyObject * 
_wrap_sf_error_number(PyObject * self, PyObject * args)
{
  /* const char*	sf_error_number	(int errnum) ;
   */
  
  int err;

  if(!PyArg_ParseTuple(args, "i:sf_error_number", &err)) {
    return NULL;
  }
  
  /* do we need to copy or deallocate the string? */
  return PyString_FromString(sf_error_number(err));
}


#define SF_CMD_LEN_IS_SIZEOF_FORMAT_INFO    1
#define SF_CMD_LEN_IS_SIZEOF_INT            2 
#define SF_CMD_LEN_IS_SIZEOF_SF_COUNT       3
#define SF_CMD_LEN_IS_LEN                   4
#define SF_CMD_LEN_IS_IGNORED               22
#define SF_CMD_LEN_IS_BOOLEAN               44

#define SF_CMD_DATA_IS_NULL                 5
#define SF_CMD_DATA_IS_OUT_INT              6
#define SF_CMD_DATA_IS_OUT_STRING           7
#define SF_CMD_DATA_IS_OUT_DOUBLE           8
#define SF_CMD_DATA_IS_OUT_DOUBLE_ARRAY_OF_LENGTH_LEN  9
#define SF_CMD_DATA_IS_INOUT_FORMAT_INFO   10
#define SF_CMD_DATA_IS_IN_SF_COUNT         11

#define SF_CMD_RETURN_IS_SUCCESS           12
#define SF_CMD_RETURN_IS_INT_COUNT         13
#define SF_CMD_RETURN_IS_BOOLEAN           14
#define SF_CMD_RETURN_IS_TWO_STRINGS       15
static PyObject * 
_wrap_sf_command(PyObject * self, PyObject * args)
{
  /* int sf_command(SNDFILE *sndfile, 
   * int command, 
   * void *data, 
   * int datasize) ;
   *
   * to make wrapping things easier, this wrapper function wants 
   * _ a sndfile cobject or None, 
   * _ an int command
   * _ description of returned value meaning
   * _ description of data attribute (in, in/out, out) and type
   * _ description of the meaning of the length 
   * _ an object or int data, etc
   * _ an integer length
   */


  PyObject * py_sfile;
  int        cmd;
  int        data_desc;
  int        ret_desc;
  int        len_desc;
  PyObject * py_dataarg;
  int        datasize;

  SNDFILE * sfile = NULL;
  void    *  data = NULL;
  int         len = 0;
  int         ret = 0;


  if(!PyArg_ParseTuple(args, "OiiiiOi:sf_command",
		       &py_sfile,
		       &cmd,
		       &data_desc,
		       &ret_desc,
		       &len_desc,
		       &py_dataarg,
		       &datasize)) { 
    return NULL;
  }
  
  if (PyCObject_Check(py_sfile)) { 
    sfile = PyCObject_AsVoidPtr(py_sfile);
  } else { 
    sfile = NULL;
  }
  
  {   
    SF_FORMAT_INFO ret_format_info;
    sf_count_t in_sf_count;
    char * ret_string;
    double ret_double;
    char  ret_int;
    double * ret_double_array;

    if (data_desc == SF_CMD_DATA_IS_OUT_STRING) { 
      len = datasize;
      data = malloc(len);
    } else if (data_desc == SF_CMD_DATA_IS_OUT_DOUBLE) { 
      len = sizeof(double);
      data = &ret_double;
    } else if (data_desc == SF_CMD_DATA_IS_OUT_DOUBLE_ARRAY_OF_LENGTH_LEN) {
      len = sizeof(double)*datasize;
      data = malloc(len);
    } else if (len_desc == SF_CMD_LEN_IS_BOOLEAN) { 
      data = NULL;
      if (datasize) { 
	len = SF_TRUE;
      } else { 
	len = SF_FALSE;
      }
    } else if (data_desc ==  SF_CMD_DATA_IS_NULL &&
	       len_desc == SF_CMD_LEN_IS_IGNORED && 
	       ret_desc == SF_CMD_RETURN_IS_BOOLEAN) {
      /* do nothing */
      data = NULL;
      datasize = 0;
    } else if (data_desc == SF_CMD_DATA_IS_OUT_INT) { 
      len = sizeof(int);
      data = &ret_int;
    } else if (data_desc == SF_CMD_DATA_IS_IN_SF_COUNT) { 
      len = sizeof(sf_count_t);
      in_sf_count = PYOBJECT_GET_SF_COUNT(py_dataarg);
      data = &in_sf_count;
    } else if  (data_desc == SF_CMD_DATA_IS_INOUT_FORMAT_INFO) { 
      len = sizeof(SF_FORMAT_INFO);
      ret_format_info.format = PyInt_AsLong(py_dataarg);
      ret_format_info.name = NULL;
      ret_format_info.extension = NULL;
      data = &ret_format_info;
    }


    ret = sf_command(sfile, cmd, data, len);

    if (ret < 0) {
      Py_INCREF(Py_None);
      return Py_None;
    } 
    if (data_desc == SF_CMD_DATA_IS_OUT_STRING) { 
      ret_string = strdup(data);
      free(data);
      return PyString_FromStringAndSize(ret_string, strlen(ret_string));    
    } else if (data_desc == SF_CMD_DATA_IS_OUT_DOUBLE) { 
      return PyFloat_FromDouble(ret_double);
    } else if (data_desc == SF_CMD_DATA_IS_OUT_DOUBLE_ARRAY_OF_LENGTH_LEN) {
      int i; 
      PyObject * tuple;
      
      ret_double_array = (double *)data;
      tuple =  PyTuple_New(datasize);
      for (i = 0; i < datasize; i++) { 
	PyTuple_SetItem(tuple, i, PyFloat_FromDouble(ret_double_array[i]));
      }
      return tuple;
    } else if ((data_desc == SF_CMD_DATA_IS_NULL ||
		data_desc == SF_CMD_DATA_IS_IN_SF_COUNT) &&
	       ret_desc == SF_CMD_RETURN_IS_SUCCESS) { 
      return PyInt_FromLong(ret);
    } else if (data_desc ==  SF_CMD_DATA_IS_NULL &&
	       ret_desc == SF_CMD_RETURN_IS_BOOLEAN) {
      if (ret == SF_TRUE) { 
	return PyInt_FromLong(1);
      } else {
	return PyInt_FromLong(0);
      }
    } else if (data_desc == SF_CMD_DATA_IS_OUT_INT) { 
      return PyInt_FromLong(ret_int);
    }  else if (ret_desc == SF_CMD_RETURN_IS_TWO_STRINGS) {
      PyObject * tuple;
      /* do i have to free the char or copy them * ? */

      if (ret != 0) { 
	  Py_INCREF(Py_None);
	  return Py_None;
      }

      tuple =  PyTuple_New(2);

      PyTuple_SetItem(tuple, 0, 
		      PyString_FromString(ret_format_info.name));
      if (ret_format_info.extension != NULL) { 
	PyTuple_SetItem(tuple, 1, 
			PyString_FromString(ret_format_info.extension));
      } else {
	PyTuple_SetItem(tuple, 1, 
			PyString_FromString(""));
      }
      return tuple;
    }

  }

  /* shouldn't ever happen */
  Py_INCREF(Py_None);
  return Py_None;

}

static PyObject * 
_wrap_sf_format_check(PyObject * self, PyObject * args)
{
  /* int sf_format_check(const SF_INFO *info) ;
   */

  PyObject * py_sfinfo;
  int    ret;
  SF_INFO   sfinfo;

  if(!PyArg_ParseTuple(args, "O:sf_format_check",
		       &py_sfinfo)) { 
    return NULL;
  }
  copy_py_sfinfo_to_sfinfo(py_sfinfo, &sfinfo);
  ret = sf_format_check (&sfinfo);
  return PyInt_FromLong(ret);
}

static PyObject * 
_wrap_sf_seek(PyObject * self, PyObject * args)
{
  /* sf_count_t	sf_seek	(SNDFILE *sndfile, 
   *                     sf_count_t frames, 
   *                     int whence) ;
   */
  SNDFILE * sfile;
  PyObject * py_sfile;
  PyObject * py_frames;
  sf_count_t frames;
  sf_count_t ret;
  int whence; 
  
  if(!PyArg_ParseTuple(args, "OOi:sf_seek",
		       &py_sfile, &py_frames, &whence)) { 
    return NULL;
  }
  sfile = PyCObject_AsVoidPtr(py_sfile);
  frames = PYOBJECT_GET_SF_COUNT(py_frames);
  ret = sf_seek(sfile, frames, whence);

  return PYOBJECT_SET_SF_COUNT(ret);
}


/* type_size should be set to match the datatype
 * multiplier is used as a flag to figure out 
 * whether or not the items should be multiplied
 * by the number of channels or not...
 * from the frames interface it should be 1, for everything else 
 * it should be 0
 */ 

#define SF_READ_WRAPPER(sf_function_name,                        \
			data_type,uses_channel_multiplier)       \
static PyObject *                                                \
_wrap_##sf_function_name (PyObject * self, PyObject * args)      \
{                                                                \
                                                                 \
  int type_size = sizeof(data_type);                             \
  int use_channel_multiplier = uses_channel_multiplier;          \
  SNDFILE * sfile = NULL;                                        \
  void * buffer;                                                 \
  sf_count_t buffer_size_bytes;                                  \
  PyObject * py_buffer;                                          \
  PyObject * py_sfile;                                           \
  PyObject * py_count;                                           \
  PyObject * py_info;                                            \
  sf_count_t count;                                              \
  sf_count_t ret;                                                \
                                                                 \
  if(!PyArg_ParseTuple(args, "OOO:"#sf_function_name,            \
		       &py_sfile, &py_count, &py_info)) {        \
    return NULL;                                                 \
  }                                                              \
  sfile = PyCObject_AsVoidPtr(py_sfile);                         \
  count = PYOBJECT_GET_SF_COUNT(py_count);                       \
                                                                 \
  buffer_size_bytes = type_size*count;                           \
  if (use_channel_multiplier) {                                  \
    SF_INFO info;                                                \
    copy_py_sfinfo_to_sfinfo(py_info, &info);                    \
    buffer_size_bytes *= info.channels;                          \
  }                                                              \
                                                                 \
  buffer = malloc(buffer_size_bytes);                            \
  ret = sf_function_name(sfile, buffer, count);                  \
                                                                 \
  /* XXX this needs a check for failure */                       \
  /* do we need to increment it ? */                             \
  /* we need to calculate the number of bytes                    \
   * that were actually returned                                 \
   */                                                            \
  ret *= type_size;                                              \
  if (use_channel_multiplier) {                                  \
    SF_INFO info;                                                \
    copy_py_sfinfo_to_sfinfo(py_info, &info);                    \
    ret *= info.channels;                                        \
  }                                                              \
                                                                 \
  py_buffer = PyString_FromStringAndSize(buffer, ret);           \
                                                                 \
  free(buffer);                                                  \
  return py_buffer;                                              \
}


#define SF_READ_NUMARRAY_WRAPPER(sf_function_name, num_array_type,\
			data_type,uses_channel_multiplier)       \
static PyObject *                                                \
_wrap_na_##sf_function_name (PyObject * self, PyObject * args)   \
{                                                                \
  int n_of_type;                                                 \
  int type_size = sizeof(data_type);                             \
  int use_channel_multiplier = uses_channel_multiplier;          \
  SNDFILE * sfile = NULL;                                        \
  void * buffer;                                                 \
  sf_count_t buffer_size_bytes;                                  \
  PyArrayObject * pao;                                           \
  PyObject * py_sfile;                                           \
  PyObject * py_count;                                           \
  PyObject * py_info;                                            \
  sf_count_t count;                                              \
  sf_count_t ret;                                                \
                                                                 \
  if(!PyArg_ParseTuple(args, "OOO:"#sf_function_name,            \
		       &py_sfile, &py_count, &py_info)) {        \
    return NULL;                                                 \
  }                                                              \
  sfile = PyCObject_AsVoidPtr(py_sfile);                         \
  count = PYOBJECT_GET_SF_COUNT(py_count);                       \
                                                                 \
  buffer_size_bytes = type_size*count;                           \
  if (use_channel_multiplier) {                                  \
    SF_INFO info;                                                \
    copy_py_sfinfo_to_sfinfo(py_info, &info);                    \
    buffer_size_bytes *= info.channels;                          \
  }                                                              \
                                                                 \
  buffer = malloc(buffer_size_bytes);                            \
  ret = sf_function_name(sfile, buffer, count);                  \
  n_of_type = ret;                                               \
  /* XXX this needs a check for failure */                       \
  /* do we need to increment it ? */                             \
  /* we need to calculate the number of bytes                    \
   * that were actually returned                                 \
   */                                                            \
  ret *= type_size;                                              \
  if (use_channel_multiplier) {                                  \
    SF_INFO info;                                                \
    copy_py_sfinfo_to_sfinfo(py_info, &info);                    \
    ret *= info.channels;                                        \
    n_of_type *= info.channels;                                  \
  }                                                              \
                                                                 \
  pao = NA_NewArray((void *)buffer, num_array_type,              \
		     1, n_of_type);                              \
  pao->flags = IS_CARRAY;\
  return (PyObject*)pao; \
}

#define SF_READ_NUMPY_WRAPPER(sf_function_name, num_py_type,  \
			data_type,uses_channel_multiplier)       \
static PyObject *                                                \
_wrap_npy_##sf_function_name (PyObject * self, PyObject * args)   \
{                                                                \
  Py_ssize_t n_of_type;                                                 \
  int type_size = sizeof(data_type);                             \
  int use_channel_multiplier = uses_channel_multiplier;          \
  SNDFILE * sfile = NULL;                                        \
  void * buffer;                                                 \
  sf_count_t buffer_size_bytes;                                  \
  PyArrayObject * pao;                                           \
  PyObject * py_sfile;                                           \
  PyObject * py_count;                                           \
  PyObject * py_info;                                            \
  sf_count_t count;                                              \
  sf_count_t ret;                                                \
  Py_ssize_t dimension[1];						 \
  if(!PyArg_ParseTuple(args, "OOO:"#sf_function_name,            \
		       &py_sfile, &py_count, &py_info)) {        \
    return NULL;                                                 \
  }                                                              \
  sfile = PyCObject_AsVoidPtr(py_sfile);                         \
  count = PYOBJECT_GET_SF_COUNT(py_count);                       \
                                                                 \
  buffer_size_bytes = type_size*count;                           \
  if (use_channel_multiplier) {                                  \
    SF_INFO info;                                                \
    copy_py_sfinfo_to_sfinfo(py_info, &info);                    \
    buffer_size_bytes *= info.channels;                          \
  }                                                              \
  								 \
  buffer = malloc(buffer_size_bytes);					\
  ret = sf_function_name(sfile, buffer, count);				\
  n_of_type = ret;							\
  /* XXX this needs a check for failure */				\
  /* do we need to increment it ? */					\
  /* we need to calculate the number of bytes				\
   * that were actually returned					\
   */									\
  ret *= type_size;							\
  if (use_channel_multiplier) {						\
    SF_INFO info;							\
    copy_py_sfinfo_to_sfinfo(py_info, &info);				\
    ret *= info.channels;						\
    n_of_type *= info.channels;						\
  }									\
  dimension[0]=n_of_type;					\
  pao = (PyArrayObject *)  PyArray_SimpleNewFromData(1,dimension, num_py_type,(char *)buffer); \
  ((PyArrayObject*)pao)->flags |= NPY_OWNDATA;				\
  return PyArray_Return(pao);						\
}


#define SF_WRITE_WRAPPER(sf_function_name,                       \
			data_type,uses_channel_multiplier)       \
static PyObject *                                                \
_wrap_##sf_function_name (PyObject * self, PyObject * args)     \
{                                                                \
                                                                 \
  int type_size = sizeof(data_type);                             \
  int use_channel_multiplier = uses_channel_multiplier;          \
  SNDFILE * sfile = NULL;                                        \
  void * buffer;                                                 \
  sf_count_t buffer_size_bytes;                                  \
  PyObject * py_strbuf;                                          \
  PyObject * py_sfile;                                           \
  PyObject * py_info;                                            \
  sf_count_t count;                                              \
  sf_count_t ret;                                                \
                                                                 \
  if(!PyArg_ParseTuple(args, "OOO:"#sf_function_name,            \
		       &py_sfile, &py_strbuf, &py_info)) {       \
    return NULL;                                                 \
  }                                                              \
  sfile = PyCObject_AsVoidPtr(py_sfile);                         \
  buffer_size_bytes = PyString_Size(py_strbuf);                  \
  buffer = PyString_AsString(py_strbuf);                         \
  /* XXX we are going to cast, because for some reason           \
   * using long long div uses stuff that python                  \
   * isn't linked against...                                     \
   */                                                            \
  count = (unsigned int)buffer_size_bytes /(unsigned int)type_size;   \
  if (use_channel_multiplier) {                                  \
    SF_INFO info;                                                \
    copy_py_sfinfo_to_sfinfo(py_info, &info);                    \
    count = (unsigned int)count/info.channels;                   \
  }                                                              \
                                                                 \
  ret = sf_function_name(sfile, buffer, count);                  \
                                                                 \
  return PYOBJECT_SET_SF_COUNT(ret);                             \
}


#define SF_WRITE_NUMARRAY_WRAPPER(sf_function_name, num_array_type,\
			data_type,uses_channel_multiplier)       \
static PyObject *                                                \
_wrap_na_##sf_function_name (PyObject * self, PyObject * args)   \
{                                                                \
                                                                 \
  int use_channel_multiplier = uses_channel_multiplier;          \
  SNDFILE * sfile = NULL;                                        \
  PyArrayObject * array;                                         \
  PyObject * py_array;                                           \
  PyObject * py_sfile;                                           \
  PyObject * py_info;                                            \
  sf_count_t count;                                              \
  sf_count_t ret;                                                \
                                                                 \
  if(!PyArg_ParseTuple(args, "OOO:"#sf_function_name,            \
		       &py_sfile, &py_array, &py_info)) {        \
    return NULL;                                                 \
  }                                                              \
  sfile = PyCObject_AsVoidPtr(py_sfile);                         \
  /* XXX: check that it is 1 dimensional */                      \
  array = NA_InputArray(py_array, num_array_type, NUM_C_ARRAY);  \
  count = array->dimensions[0];                                  \
  /* XXX we are going to cast, because for some reason           \
   * using long long div uses stuff that python                  \
   * isn't linked against...                                     \
   */                                                            \
  if (use_channel_multiplier) {                                  \
    SF_INFO info;                                                \
    copy_py_sfinfo_to_sfinfo(py_info, &info);                    \
    count = (unsigned int)count/info.channels;                   \
  }                                                              \
                                                                 \
  ret = sf_function_name(sfile, (data_type*)array->data, count); \
  /* release the ref created by the InputArray call */           \
  Py_XDECREF(array);                                             \
  return PYOBJECT_SET_SF_COUNT(ret);                             \
}

#define SF_WRITE_NUMPY_WRAPPER(sf_function_name, num_py_type,\
			data_type,uses_channel_multiplier)       \
static PyObject *                                                \
_wrap_npy_##sf_function_name (PyObject * self, PyObject * args)   \
{                                                                \
                                                                 \
  int use_channel_multiplier = uses_channel_multiplier;          \
  SNDFILE * sfile = NULL;                                        \
  PyArrayObject * array;                                         \
  PyObject * py_array;                                           \
  PyObject * py_sfile;                                           \
  PyObject * py_info;                                            \
  sf_count_t count;                                              \
  sf_count_t ret;                                                \
                                                                 \
  if(!PyArg_ParseTuple(args, "OOO:"#sf_function_name,            \
		       &py_sfile, &py_array, &py_info)) {        \
    return NULL;                                                 \
  }                                                              \
  sfile = PyCObject_AsVoidPtr(py_sfile);                         \
  /* XXX: check that it is 1 dimensional */                      \
  array = (PyArrayObject *) PyArray_ContiguousFromObject(py_array, num_py_type,0,2); \
  count = array->dimensions[0];                                  \
  /* XXX we are going to cast, because for some reason           \
   * using long long div uses stuff that python                  \
   * isn't linked against...                                     \
   */                                                            \
  if (use_channel_multiplier) {                                  \
    SF_INFO info;                                                \
    copy_py_sfinfo_to_sfinfo(py_info, &info);                    \
    count = (unsigned int)count/info.channels;                   \
  }                                                              \
                                                                 \
  ret = sf_function_name(sfile, (data_type*)array->data, count); \
  /* release the ref created by the InputArray call */           \
  Py_XDECREF(array);                                             \
  return PYOBJECT_SET_SF_COUNT(ret);                             \
}



SF_READ_WRAPPER(sf_read_raw, char, 0);
SF_WRITE_WRAPPER(sf_write_raw, char, 0);

SF_READ_WRAPPER(sf_readf_short, short, 1);
SF_WRITE_WRAPPER(sf_writef_short, short, 1);
SF_READ_WRAPPER(sf_readf_int, int, 1);
SF_WRITE_WRAPPER(sf_writef_int, int, 1);
SF_READ_WRAPPER(sf_readf_float, float, 1);
SF_WRITE_WRAPPER(sf_writef_float, float, 1);
SF_READ_WRAPPER(sf_readf_double, double, 1);
SF_WRITE_WRAPPER(sf_writef_double, double, 1);

SF_READ_WRAPPER(sf_read_short, short, 0);
SF_WRITE_WRAPPER(sf_write_short, short, 0);
SF_READ_WRAPPER(sf_read_int, int, 0);
SF_WRITE_WRAPPER(sf_write_int, int, 0);
SF_READ_WRAPPER(sf_read_float, float, 0);
SF_WRITE_WRAPPER(sf_write_float, float, 0);
SF_READ_WRAPPER(sf_read_double, double, 0);
SF_WRITE_WRAPPER(sf_write_double, double, 0);


#ifdef HAVE_NUMARRAY
/* numarray versions */
/* XXX we need ifdefs to check on datatypes  */
SF_READ_NUMARRAY_WRAPPER(sf_read_raw, tInt8, char, 0);
SF_WRITE_NUMARRAY_WRAPPER(sf_write_raw, tInt8, char, 0);


SF_READ_NUMARRAY_WRAPPER(sf_readf_short, tInt16, short, 1);
SF_READ_NUMARRAY_WRAPPER(sf_readf_int, tInt32, int, 1);
SF_READ_NUMARRAY_WRAPPER(sf_readf_float, tFloat32, float, 1);
SF_READ_NUMARRAY_WRAPPER(sf_readf_double, tFloat64, double, 1);


SF_READ_NUMARRAY_WRAPPER(sf_read_short, tInt16, short, 0);
SF_READ_NUMARRAY_WRAPPER(sf_read_int, tInt32, int, 0);
SF_READ_NUMARRAY_WRAPPER(sf_read_float, tFloat32, float, 0);
SF_READ_NUMARRAY_WRAPPER(sf_read_double, tFloat64, double, 0);

SF_WRITE_NUMARRAY_WRAPPER(sf_writef_short, tInt16, short, 1);
SF_WRITE_NUMARRAY_WRAPPER(sf_writef_int, tInt32, int, 1);
SF_WRITE_NUMARRAY_WRAPPER(sf_writef_float, tFloat32, float, 1);
SF_WRITE_NUMARRAY_WRAPPER(sf_writef_double, tFloat64, double, 1);


SF_WRITE_NUMARRAY_WRAPPER(sf_write_short, tInt16, short, 0);
SF_WRITE_NUMARRAY_WRAPPER(sf_write_int, tInt32, int, 0);
SF_WRITE_NUMARRAY_WRAPPER(sf_write_float, tFloat32, float, 0);
SF_WRITE_NUMARRAY_WRAPPER(sf_write_double, tFloat64, double, 0);
#endif

#ifdef HAVE_NUMPY
/* numarray versions */
/* XXX we need ifdefs to check on datatypes  */
SF_READ_NUMPY_WRAPPER(sf_read_raw, PyArray_CHAR, char, 0);
SF_WRITE_NUMPY_WRAPPER(sf_write_raw, PyArray_CHAR, char, 0);


SF_READ_NUMPY_WRAPPER(sf_readf_short, PyArray_SHORT, short, 1);
SF_READ_NUMPY_WRAPPER(sf_readf_int, PyArray_INT, int, 1);
SF_READ_NUMPY_WRAPPER(sf_readf_float, PyArray_FLOAT, float, 1);
SF_READ_NUMPY_WRAPPER(sf_readf_double, PyArray_DOUBLE, double, 1);


SF_READ_NUMPY_WRAPPER(sf_read_short, PyArray_SHORT, short, 0);
SF_READ_NUMPY_WRAPPER(sf_read_int, PyArray_INT, int, 0);
SF_READ_NUMPY_WRAPPER(sf_read_float, PyArray_FLOAT, float, 0);
SF_READ_NUMPY_WRAPPER(sf_read_double, PyArray_DOUBLE, double, 0);

SF_WRITE_NUMPY_WRAPPER(sf_writef_short, PyArray_SHORT, short, 1);
SF_WRITE_NUMPY_WRAPPER(sf_writef_int, PyArray_INT, int, 1);
SF_WRITE_NUMPY_WRAPPER(sf_writef_float, PyArray_FLOAT, float, 1);
SF_WRITE_NUMPY_WRAPPER(sf_writef_double, PyArray_DOUBLE, double, 1);


SF_WRITE_NUMPY_WRAPPER(sf_write_short, PyArray_SHORT, short, 0);
SF_WRITE_NUMPY_WRAPPER(sf_write_int, PyArray_INT, int, 0);
SF_WRITE_NUMPY_WRAPPER(sf_write_float, PyArray_FLOAT, float, 0);
SF_WRITE_NUMPY_WRAPPER(sf_write_double, PyArray_DOUBLE, double, 0);
#endif


static PyMethodDef methods[] = {
  { "_wrap_sf_open", _wrap_sf_open, METH_VARARGS },
  { "_wrap_sf_close", _wrap_sf_close, METH_VARARGS },
  { "_wrap_sf_error", _wrap_sf_error, METH_VARARGS },
  { "_wrap_sf_strerror", _wrap_sf_strerror, METH_VARARGS },
  { "_wrap_sf_error_number", _wrap_sf_error_number, METH_VARARGS },
  { "_wrap_sf_command", _wrap_sf_command, METH_VARARGS },
  { "_wrap_sf_format_check", _wrap_sf_format_check, METH_VARARGS },  
  { "_wrap_sf_seek", _wrap_sf_seek, METH_VARARGS },

  { "_wrap_sf_read_raw", _wrap_sf_read_raw, METH_VARARGS },
  { "_wrap_sf_write_raw", _wrap_sf_write_raw, METH_VARARGS },  

  { "_wrap_sf_readf_short", _wrap_sf_readf_short, METH_VARARGS },
  { "_wrap_sf_writef_short", _wrap_sf_writef_short, METH_VARARGS },  
  { "_wrap_sf_readf_int", _wrap_sf_readf_int, METH_VARARGS },
  { "_wrap_sf_writef_int", _wrap_sf_writef_int, METH_VARARGS },  
  { "_wrap_sf_readf_float", _wrap_sf_readf_float, METH_VARARGS },
  { "_wrap_sf_writef_float", _wrap_sf_writef_float, METH_VARARGS },
  { "_wrap_sf_readf_double", _wrap_sf_readf_double, METH_VARARGS },
  { "_wrap_sf_writef_double", _wrap_sf_writef_double, METH_VARARGS },  

  { "_wrap_sf_read_short", _wrap_sf_read_short, METH_VARARGS },
  { "_wrap_sf_write_short", _wrap_sf_write_short, METH_VARARGS },  
  { "_wrap_sf_read_int", _wrap_sf_read_int, METH_VARARGS },
  { "_wrap_sf_write_int", _wrap_sf_write_int, METH_VARARGS },  
  { "_wrap_sf_read_float", _wrap_sf_read_float, METH_VARARGS },
  { "_wrap_sf_write_float", _wrap_sf_write_float, METH_VARARGS },
  { "_wrap_sf_read_double", _wrap_sf_read_double, METH_VARARGS },
  { "_wrap_sf_write_double", _wrap_sf_write_double, METH_VARARGS },  


#ifdef HAVE_NUMARRAY
  { "_wrap_na_sf_read_raw", _wrap_na_sf_read_raw, METH_VARARGS },
  { "_wrap_na_sf_write_raw", _wrap_na_sf_write_raw, METH_VARARGS },  


  { "_wrap_na_sf_readf_short", _wrap_na_sf_readf_short, METH_VARARGS },
  { "_wrap_na_sf_readf_int", _wrap_na_sf_readf_int, METH_VARARGS },
  { "_wrap_na_sf_readf_float", _wrap_na_sf_readf_float, METH_VARARGS },
  { "_wrap_na_sf_readf_double", _wrap_na_sf_readf_double, METH_VARARGS },
  { "_wrap_na_sf_read_short", _wrap_na_sf_read_short, METH_VARARGS },
  { "_wrap_na_sf_read_int", _wrap_na_sf_read_int, METH_VARARGS },
  { "_wrap_na_sf_read_float", _wrap_na_sf_read_float, METH_VARARGS },
  { "_wrap_na_sf_read_double", _wrap_na_sf_read_double, METH_VARARGS },  

  { "_wrap_na_sf_writef_short", _wrap_na_sf_writef_short, METH_VARARGS },
  { "_wrap_na_sf_writef_int", _wrap_na_sf_writef_int, METH_VARARGS },
  { "_wrap_na_sf_writef_float", _wrap_na_sf_writef_float, METH_VARARGS },
  { "_wrap_na_sf_writef_double", _wrap_na_sf_writef_double, METH_VARARGS },
  { "_wrap_na_sf_write_short", _wrap_na_sf_write_short, METH_VARARGS },
  { "_wrap_na_sf_write_int", _wrap_na_sf_write_int, METH_VARARGS },
  { "_wrap_na_sf_write_float", _wrap_na_sf_write_float, METH_VARARGS },
  { "_wrap_na_sf_write_double", _wrap_na_sf_write_double, METH_VARARGS },  

#endif
#ifdef HAVE_NUMPY
  { "_wrap_npy_sf_read_raw", _wrap_npy_sf_read_raw, METH_VARARGS },
  { "_wrap_npy_sf_write_raw", _wrap_npy_sf_write_raw, METH_VARARGS },  


  { "_wrap_npy_sf_readf_short", _wrap_npy_sf_readf_short, METH_VARARGS },
  { "_wrap_npy_sf_readf_int", _wrap_npy_sf_readf_int, METH_VARARGS },
  { "_wrap_npy_sf_readf_float", _wrap_npy_sf_readf_float, METH_VARARGS },
  { "_wrap_npy_sf_readf_double", _wrap_npy_sf_readf_double, METH_VARARGS },
  { "_wrap_npy_sf_read_short", _wrap_npy_sf_read_short, METH_VARARGS },
  { "_wrap_npy_sf_read_int", _wrap_npy_sf_read_int, METH_VARARGS },
  { "_wrap_npy_sf_read_float", _wrap_npy_sf_read_float, METH_VARARGS },
  { "_wrap_npy_sf_read_double", _wrap_npy_sf_read_double, METH_VARARGS },  

  { "_wrap_npy_sf_writef_short", _wrap_npy_sf_writef_short, METH_VARARGS },
  { "_wrap_npy_sf_writef_int", _wrap_npy_sf_writef_int, METH_VARARGS },
  { "_wrap_npy_sf_writef_float", _wrap_npy_sf_writef_float, METH_VARARGS },
  { "_wrap_npy_sf_writef_double", _wrap_npy_sf_writef_double, METH_VARARGS },
  { "_wrap_npy_sf_write_short", _wrap_npy_sf_write_short, METH_VARARGS },
  { "_wrap_npy_sf_write_int", _wrap_npy_sf_write_int, METH_VARARGS },
  { "_wrap_npy_sf_write_float", _wrap_npy_sf_write_float, METH_VARARGS },
  { "_wrap_npy_sf_write_double", _wrap_npy_sf_write_double, METH_VARARGS },  
#endif
  { NULL, NULL },
};

void
init_libsndfile(void)
{
    PyObject *m, *d;
#ifdef HAVE_NUMARRAY
    import_libnumarray();
#endif
#ifdef HAVE_NUMPY
        import_array();
#endif

    m = Py_InitModule("_libsndfile", methods);
    d = PyModule_GetDict(m);
}
